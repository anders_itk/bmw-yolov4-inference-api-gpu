from semanticmap import HomographicProjection
import numpy as np
from scipy.spatial.transform import Rotation as R

class Projection():
    def __init__(self, angle: int, height: float, focal_length_px: int, image_size: tuple) -> list:
        """
        This script is a wrapper for aau sematic map and returns the coordinate estimation
        Based on sematicmap.py
        https://github.com/kdhansen/aau_semantic_map/commit/03557d42204d330116f5e3cc6983a0bb68eb00ad


        :param angle: camera angle in degrees. E.g. 45, if pointing downwards at 45 degrees angle. 90 = straight at the ground.
        :param height: camera height over the ground in meters. E.g. 1.11 if camera is 1 m, 11cm over the ground.
        :param focal_length_px: calculate with the formula based on camera specifications.
        :param image_size: tuple (w,h) image resolution in pixels. E.g. (800,600).
        :return list of coordinates in meters [x,y,z]. E.g. [0.9934693877551017, 0.03463380152750449, 1.1102230246251565e-16]
        """
        if not 0 <= angle <= 360:
            raise ValueError(f"Angle must be between 0 and 360. Got '{angle}'.")

        w = image_size[0] / 2
        h = image_size[1] / 2

        cam_transform = np.identity(4)
        cam_transform[0:3, 3] = [0, 0, height]
        rot_C = R.from_rotvec(np.pi / 4 * np.array([0, 1, 0]))
        cam_transform[0:3, 0:3] = rot_C.as_matrix()

        plane_transform = np.identity(4)
        intrinsics = np.array([[focal_length_px, 0, w],
                               [0, focal_length_px, h],
                               [0, 0, 1]])

        self.H = HomographicProjection(cam_transform, plane_transform, intrinsics)

    def get_point_in_plane(self, pixel: tuple):
        """
        :param pixel: pixel of interest, e.g. center of cigarette butt. E.g. (300,89)
        :return: point in plane as tuple (x,y,z) in meters, relative to the camera.
        """
        return self.H.project_to_plane(pixel)

    def augment_prediction(self, data: dict) -> dict:
        """
        :param data: model prediction from inference. Expected shape data = dict(data=dict(bounding-boxes=[dict(),dict(),...]))
        :return: inference data with added point-in-plane data
        """
        for image_object in data['bounding-boxes']:
            # calculate center pixel for each detected object
            box_center = ((image_object['coordinates']['left'] + image_object['coordinates']['right'])/2,
                          (image_object['coordinates']['top'] + image_object['coordinates']['bottom'])/2)
            image_object['point_in_plane'] = self.get_point_in_plane(pixel=box_center)

        return data
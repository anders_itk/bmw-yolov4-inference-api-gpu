from skod_config import Skod
import os
import json


class Persist():
    """
    Store inference data, image and gnss coordinates to filesystem.
    On init the directores are created, if necessary.
    """
    def __init__(self):
        self.img_path = Skod.PARENT_DIR + os.sep + Skod.IMG_DIR
        self.gnss_path = Skod.PARENT_DIR + os.sep + Skod.GNSS_DIR
        self.infer_path = Skod.PARENT_DIR + os.sep + Skod.INFERENCE_DIR
        try:
            os.makedirs(self.img_path)
        except FileExistsError:
            pass
        try:
            os.makedirs(self.gnss_path)
        except FileExistsError:
            pass
        try:
            os.makedirs(self.infer_path)
        except FileExistsError:
            pass

    def write_to_disk(self, label: str, gnss: str, img, inference: dict):
        """
        Write data to disk in designated directories.

        :param label: string with the name used for each file. E.g. "20210101_00.00.00".
        :param gnss: lat/long as string
        :param img: binary jpg image
        :param inference: json formatted inference results
        :return: None
        """
        with open(self.gnss_path + os.sep + label + ".txt", 'w') as f:
            f.write(gnss)

        with open(self.img_path + os.sep + label + ".jpg", 'wb') as f:
            f.write(img.read())

        with open(self.infer_path + os.sep + label + ".json", 'w') as f:
            f.write(json.dumps(inference))

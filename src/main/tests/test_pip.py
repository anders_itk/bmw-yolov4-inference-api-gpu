import unittest

class MyTestCase(unittest.TestCase):
    def test_point_in_plane(self):
        """
        Given the inference response with 1 or more detected objects, then the function should run witout error
        """
        import time
        start = time.time()
        from src.main.calculate_location import Projection
        import tests.test_data.inference_results as ir

        # image prediction
        test_data = ir.Prediction().data

        # arbitrary camera info
        projection = Projection(angle=70,image_size=(1200,676),focal_length_px=1221,height=0.3)
        result_test_data = projection.augment_prediction(test_data)
        print(f"runtime: {time.time()-start}")
        self.assertIn('point_in_plane', result_test_data['bounding-boxes'][0].keys())


if __name__ == '__main__':
    unittest.main()

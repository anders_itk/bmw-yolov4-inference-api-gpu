import json
class Prediction():
    """
    Endpoint: http://74.82.29.29:57744/models/cig/predict
    Image: cigsongravel.jpg
    """
    data = json.loads("""{
        "bounding-boxes": [
          {
            "ObjectClassId": 0,
            "ObjectClassName": "cigarette",
            "confidence": 96.96240425109863,
            "coordinates": {
              "left": 915.6347885131836,
              "top": 0.4010944366455078,
              "right": 1014.4498062133789,
              "bottom": 55.99081230163574
            }
          },
          {
            "ObjectClassId": 0,
            "ObjectClassName": "cigarette",
            "confidence": 96.54331803321838,
            "coordinates": {
              "left": 839.5724983215332,
              "top": 147.9075927734375,
              "right": 905.6928825378418,
              "bottom": 227.87896728515625
            }
          },
          {
            "ObjectClassId": 0,
            "ObjectClassName": "cigarette",
            "confidence": 87.8477931022644,
            "coordinates": {
              "left": 1115.8422775268555,
              "top": 341.6694869995117,
              "right": 1195.7155838012695,
              "bottom": 426.3646926879883
            }
          },
          {
            "ObjectClassId": 0,
            "ObjectClassName": "cigarette",
            "confidence": 85.86869239807129,
            "coordinates": {
              "left": 261.9801712036133,
              "top": 273.0989303588867,
              "right": 361.42858123779297,
              "bottom": 353.31189727783203
            }
          }
        ],
      "error": "",
      "success": true
    }""")